package com.mekari.mokaaddons.webhookhandler.common.event;

public interface EventBody {
    String getId();
}
