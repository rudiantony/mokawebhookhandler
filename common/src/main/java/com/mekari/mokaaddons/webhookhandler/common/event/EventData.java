package com.mekari.mokaaddons.webhookhandler.common.event;

public interface EventData {
    public String getId();
}
